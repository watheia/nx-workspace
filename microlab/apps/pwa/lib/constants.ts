/**
 * Copyright 2021 Watheia Labs, LLC..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const SITE_URL = "https://pwa-watheia.vercel.app"
export const SITE_ORIGIN = process.env.NEXT_PUBLIC_SITE_ORIGIN || new URL(SITE_URL).origin
export const TWITTER_USER_NAME = "watheia"
export const BRAND_NAME = "Watheia"
export const SITE_NAME_MULTILINE = ["Watheia", "Labs"]
export const SITE_NAME = "Watheia Labs"
export const META_DESCRIPTION =
  "Watheia Labs, LLC. is a modern engineering and digital design agency offering consulting services in Southeast Washington State."
export const SITE_DESCRIPTION = "Micro Frontends by Watheia Labs"
export const DATE = "June 1st, 2021"
export const SHORT_DATE = "Jun 1 - 9:00am PST"
export const FULL_DATE = "Jun 1st 9am Pacific Time (GMT-7)"
export const TWEET_TEXT = META_DESCRIPTION
export const COOKIE = "user-id"

// Remove process.env.NEXT_PUBLIC_... below and replace them with
// strings containing your own privacy policy URL and copyright holder name
export const LEGAL_URL = process.env.NEXT_PUBLIC_PRIVACY_POLICY_URL
export const COPYRIGHT_HOLDER = process.env.NEXT_PUBLIC_COPYRIGHT_HOLDER

export const CODE_OF_CONDUCT = "https://cdn.watheia.org/assets/terms-and-conditions.txt"
export const REPO = "https://github.com/drkstr101/datocms-pwa-watheia"
export const SAMPLE_TICKET_NUMBER = 1234
export const NAVIGATION = [
  {
    name: "zone_a",
    route: "/stage/a"
  },
  {
    name: "zone_b",
    route: "/stage/b"
  },
  {
    name: "zone_c",
    route: "/stage/c"
  },
  {
    name: "zone_d",
    route: "/stage/d"
  },
  {
    name: "zone_e",
    route: "/stage/e"
  },
  {
    name: "zone_m",
    route: "/stage/m"
  },
  {
    name: "About",
    route: "/schedule"
  },
  {
    name: "Micro",
    route: "/speakers"
  },
  {
    name: "News",
    route: "/expo"
  },
  {
    name: "Contact",
    route: "/tickets"
  }
]

export type TicketGenerationState = "default" | "loading"
