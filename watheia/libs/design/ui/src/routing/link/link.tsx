import React from 'react';
import type { LinkProps } from '@watheia/design.ui.routing.native-link';
import { useRouting } from '@watheia/design.ui.routing.routing-provider';

export type { LinkProps };

export function Link(props: LinkProps) {
  const ActualLink = useRouting().Link;
  return <ActualLink {...props} />;
}
