import React from 'react';
import type { NavLinkProps } from '@watheia/design.ui.routing.native-nav-link';
import { useRouting } from '@watheia/design.ui.routing.routing-provider';

export type { NavLinkProps };

export function NavLink(props: NavLinkProps) {
  const ActualNavLink = useRouting().NavLink;
  return <ActualNavLink {...props} />;
}
