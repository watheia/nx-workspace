export * from '@watheia/design.ui.surfaces.split-pane.layout';
export * from '@watheia/design.ui.surfaces.split-pane.pane';
export * from '@watheia/design.ui.surfaces.split-pane.splitter';

export { SplitPane } from './split-pane';
export type { Size, SplitPaneProps } from './split-pane';
